"""
Django settings for intexdental project.

Generated by 'django-admin startproject' using Django 2.2.6.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '5e6$$1-&jc3mp*hnc_8rt_75)t@4rpgw+e7^ulwcjqgf$!oki+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['intex-dental.com']

# Application definition

INSTALLED_APPS = [
    'modeltranslation',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

CUSTOM_APP = [
    'backend.main',
    'backend.meta'
]

VENDOR_APP = [
    'rosetta',
    'ckeditor',
    'ckeditor_uploader',
    'solo', 
'adminsortable'
    # 'django_telegrambot',
    # 'rest_framework'
]

# DJANGO_TELEGRAMBOT = {
#
#     'MODE': 'POLLING',  # (Optional [str]) # The default value is WEBHOOK,
#     # otherwise you may use 'POLLING'
#     # NB: if use polling you must provide to run
#     # a management command that starts a worker
#
#     'WEBHOOK_SITE': 'http://localhost:8000',
#     # 'WEBHOOK_PREFIX': '/prefix',  # (Optional[str]) # If this value is specified,
#     # a prefix is added to webhook url
#
#     # 'WEBHOOK_CERTIFICATE' : 'cert.pem', # If your site use self-signed
#     # certificate, must be set with location of your public key
#     # certificate.(More info at https://core.telegram.org/bots/self-signed )
#
#     'BOTS': [
#         {
#             'TOKEN': '1005872507:AAHwfrZzRDPMGTo5ocQWh9U8OY70dI6e-bI',  # Your bot token.
#
#             # 'ALLOWED_UPDATES':(Optional[list[str]]), # List the types of
#             # updates you want your bot to receive. For example, specify
#             # ``["message", "edited_channel_post", "callback_query"]`` to
#             # only receive updates of these types. See ``telegram.Update``
#             # for a complete list of available update types.
#             # Specify an empty list to receive all updates regardless of type
#             # (default). If not specified, the previous setting will be used.
#             # Please note that this parameter doesn't affect updates created
#             # before the call to the setWebhook, so unwanted updates may be
#             # received for a short period of time.
#
#             # 'TIMEOUT':(Optional[int|float]), # If this value is specified,
#             # use it as the read timeout from the server
#
#             # 'WEBHOOK_MAX_CONNECTIONS':(Optional[int]), # Maximum allowed number of
#             # simultaneous HTTPS connections to the webhook for update
#             # delivery, 1-100. Defaults to 40. Use lower values to limit the
#             # load on your bot's server, and higher values to increase your
#             # bot's throughput.
#
#             # 'POLL_INTERVAL' : (Optional[float]), # Time to wait between polling updates from Telegram in
#             # seconds. Default is 0.0
#
#             # 'POLL_CLEAN':(Optional[bool]), # Whether to clean any pending updates on Telegram servers before
#             # actually starting to poll. Default is False.
#
#             # 'POLL_BOOTSTRAP_RETRIES':(Optional[int]), # Whether the bootstrapping phase of the `Updater`
#             # will retry on failures on the Telegram server.
#             # |   < 0 - retry indefinitely
#             # |     0 - no retries (default)
#             # |   > 0 - retry up to X times
#
#             # 'POLL_READ_LATENCY':(Optional[float|int]), # Grace time in seconds for receiving the reply from
#             # server. Will be added to the `timeout` value and used as the read timeout from
#             # server (Default: 2).
#         },
#         # Other bots here with same structure.
#     ],
#
# }

INSTALLED_APPS += CUSTOM_APP
INSTALLED_APPS += VENDOR_APP

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',  # locale add
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'intexdental.urls'
SETTINGS_PATH = os.path.normpath(os.path.dirname(__file__))

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(SETTINGS_PATH, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'intexdental.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#    }
#}
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'intex',
        "USER": 'root',
        'PASSWORD': 'adminadmin',
        'HOST': 'localhost',
        'PORT': '3306'
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# locale settings


gettext = lambda s: s

LANGUAGES = (
    ('ua', gettext('Ukraine')),
    ('ru', gettext('Russian')),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = True
ROSETTA_SHOW_AT_ADMIN_PANEL = True
MODELTRANSLATION_FALLBACK_LANGUAGES = ('ua', 'ru')

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static")
    # 'www/static/',
]

STATIC_URL = '/static/'
STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR, '../static'))

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, '../media')

CKEDITOR_UPLOAD_PATH = "uploads/"

DATA_UPLOAD_MAX_MEMORY_SIZE = 524288000

# CKEDITOR_UPLOAD_PATH =os.path.join(BASE_DIR, 'uploads/')

# CKEDITOR_UPLOAD_SLUGIFY_FILENAME = False
# CKEDITOR_JQUERY_URL = 'http://libs.baidu.com/jquery/2.0.3/jquery.min.js'
# CKEDITOR_IMAGE_BACKEND = "pillow"
# CKEDITOR_UPLOAD_SLUGIFY_FILENAME = True
# CKEDITOR_IMAGE_BACKEND = "pillow"


CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': (
            ['div', 'Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'],
            ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
            ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
            ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
            ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink', 'Anchor'],
            # ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
            ['Image', 'Update', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
            ['Styles', 'Format', 'Font', 'FontSize'],
            ['TextColor', 'BGColor'],
            ['Maximize', 'ShowBlocks', '-', 'About', 'pbckcode', 'Video', 'Youtube', 'iframe'],
        ),
    }
}

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    # other middleware classes
    'django.middleware.cache.FetchFromCacheMiddleware',
    'htmlmin.middleware.MarkRequestMiddleware',
)

HTML_MINIFY = True
KEEP_COMMENTS_ON_MINIFYING = True
try:
    from .local import *
except ImportError as e:
    print(e)
