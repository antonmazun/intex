import os
import django
import random
from datetime import timedelta, datetime

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "intexdental.settings")
SECRET_KEY = os.environ.get('SECRET_KEY')
django.setup()

from backend.main.models import Article, Doctor


def main():
    print(Article)


def create_articles():
    def random_date(start, end):
        delta = end - start
        int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
        random_second = random.randrange(int_delta)
        return start + timedelta(seconds=random_second)

    with open("random_text.txt", encoding='utf-8') as f:
        data = f.read().split(' ')
        all_doctors = Doctor.objects.filter(status=True).values_list('id', flat=True)

        print(all_doctors)
        try:
            for elem in range(100):
                new_article = Article(
                    author_id=random.choice(list(all_doctors)),
                    title=' '.join(random.sample(data, 3)),
                    lead=' '.join(random.sample(data, 12)),
                    content=' '.join(random.sample(data, 50)),
                    status=True
                )
                new_article.save()
        except Exception as e:
            print('ERRORORORORO!!')
            print(e)

        # print(data)
    # print(users_list)


if __name__ == '__main__':
    create_articles()
