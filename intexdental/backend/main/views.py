from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .forms import CallBackFormFront
from htmlmin.decorators import minified_response
from .models import Sale, Category, Doctor, CallBackForm, Article, StaticInfo, Review
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.translation import ugettext_lazy as _, ugettext_noop as _noop
# Create your views here.

import requests

BOT_TOKEN = '1150204605:AAH6eqYQM5lBhFPjWp1WoaLHoubWOIxfUn0'
BOT_CHAT_ID = '-467576581'


def get_sales():
    return Sale.objects.filter(status=1)


@minified_response
def homepage(request):
    ctx = {
        'page': 'home'
    }
    ctx['sales'] = get_sales()

    return render(request, 'main/home.html', ctx)


@minified_response
def reviewpage(request):
    ctx = {
        'page': 'review'
    }
    bread = [(_('Головна'), 'main:home'), (_('Отзывы'), '')]
    ctx['bredcrumbs'] = bread
    if request.method == 'POST':
        name = request.POST.get('name')
        text = request.POST.get('text')
        msg_to_telegramm = 'Отзыв \nІм’я: {}\n text: {}'.format(name, text)
        send_text = 'https://api.telegram.org/bot' \
                    + BOT_TOKEN + '/sendMessage?chat_id=' \
                    + BOT_CHAT_ID + '&parse_mode=Markdown&text=' + msg_to_telegramm
        requests.get(send_text)
        Review.objects.create(
            name=name,
            text=text,
        )
        return JsonResponse({
            'status': True
        })
    return render(request, 'main/review.html', ctx)


@minified_response
def gallerypage(request):
    ctx = {
        'page': 'gallery'
    }
    bread = [(_('Головна'), 'main:home'), (_('Галерея'), '')]
    ctx['bredcrumbs'] = bread
    return render(request, 'main/gallery.html', ctx)


@minified_response
def questionspage(request):
    ctx = {
        'page': 'questions'
    }
    bread = [(_('Головна'), 'main:home'), (_('Частые вопросы'), '')]
    ctx['bredcrumbs'] = bread
    return render(request, 'main/questions.html', ctx)


@minified_response
def contactspage(request):
    ctx = {
        'page': 'contacts'
    }

    return render(request, 'main/contacts.html', ctx)


@minified_response
def pricepage(request):
    ctx = {}
    ctx.update({
        'page': 'price'
    })
    categories = Category.objects.filter(status=True)
    ctx['categories'] = categories
    bread = [(_('Головна'), 'main:home'), (_('Услуги'), '')]
    ctx['bredcrumbs'] = bread
    return render(request, 'main/price.html', ctx)


@minified_response
def servicepage(request, slug):
    ctx = {}
    ctx['page'] = 'service'
    bread = [('Головна', 'main:home')]

    try:
        service = StaticInfo.objects.get(slug=slug)
        ctx['service'] = service
        bread.append((service.name, ''))
    except Exception as e:
        print(type(e), e)
    ctx['bredcrumbs'] = bread
    return render(request, 'main/service.html', ctx)


@minified_response
def doctorspage(request):
    ctx = {
        'page': 'doctors'
    }
    bread = [('Головна', 'main:home'), ('Наша команда', '')]
    ctx['bredcrumbs'] = bread
    return render(request, 'main/doctors.html', ctx)


@minified_response
def doctorpage(request, slug):
    ctx = {
        'page': 'doctor'
    }
    try:
        doctor = Doctor.objects.get(slug=slug)
        ctx['doctor'] = doctor
        bread = [
            ('Головна', 'main:home'),
            ('Наша команда', 'main:doctors'),
            (doctor.fio, ''),
        ]
        ctx['bredcrumbs'] = bread
    except Exception as e:
        print(type(e), e)
    return render(request, 'main/doctor.html', ctx)


@minified_response
def articlespage(request):
    ctx = {
        'page': 'articles'
    }
    all_articles = Article.objects.filter(status=True).order_by('-id')

    page = request.GET.get('page', 1)

    paginator = Paginator(all_articles, 6)
    try:
        all_articles = paginator.page(page)
    except PageNotAnInteger:
        all_articles = paginator.page(1)
    except EmptyPage:
        all_articles = paginator.page(paginator.num_pages)

    ctx['all_articles'] = all_articles
    bread = [('Головна', 'main:home'), ('Статьи', '')]
    ctx['bredcrumbs'] = bread
    return render(request, 'main/articles.html', ctx)


@minified_response
def articlepage(request, slug):
    ctx = {
        'page': 'article'
    }
    try:
        article = Article.objects.get(slug=slug)
        ctx['article'] = article
        bread = [('Головна', 'main:home'), ('Статьи', 'main:articles')]
        bread.append((article.title, ''))
        ctx['bredcrumbs'] = bread
    except Exception as e:
        print(type(e), e)

    return render(request, 'main/article.html', ctx)


@minified_response
def stockspage(request):
    bread = [('Головна', 'main:home')]
    bread.append(('Акции', ''))
    ctx = {
        'page': 'stocks',
        'bredcrumbs': bread
    }

    return render(request, 'main/stocks.html', ctx)


@minified_response
def form(request):
    ctx = {}
    # if request.method == 'POST':
    form = CallBackFormFront(request.POST)
    if form.is_valid():
        form.save()
        print('asdasdas')
        name = form.cleaned_data['name_user']
        phone_number = form.cleaned_data['phone_number']
        msg = form.cleaned_data['msg']
        msg_to_telegramm = 'Заявка \nІм’я: {}\n Номер телефона: {} \n Сообщение: {}'.format(name, phone_number, msg)
        send_text = 'https://api.telegram.org/bot' + BOT_TOKEN + '/sendMessage?chat_id=' + BOT_CHAT_ID + '&parse_mode=Markdown&text=' + msg_to_telegramm
        requests.get(send_text)
        return redirect('main:thanks')
    else:
        return JsonResponse({
            'status': False
        })


def thanks(request):
    return render(request, 'thanks.html', {
        "page": 'thanks'
    })


@minified_response
def callback_full(request):
    name = request.POST.get('name')
    phone = request.POST.get('phone')
    text = request.POST.get('text')
    service_id = request.POST.get('service_id')
    doctor_id = None
    try:
        # service_id = int(request.POST.get('service_id'))
        doctor_id = int(request.POST.get('doctor_id'))
    except Exception as e:
        print(type(e), e)
    form = CallBackForm()
    form.name_user = name
    form.phone_number = phone
    form.msg = text
    form.service_1 = service_id
    # form.service_id = service_id if service_id != -1 else None
    form.doctor_id = doctor_id if doctor_id != -1 else None
    form.save()
    msg_to_telegramm = 'Заявка \nІм’я: {}\nНомер телефона: {} \nУслуга : {} \nСообщение: {}'.format(name, phone,
                                                                                                    [value for
                                                                                                     key, value in
                                                                                                     CallBackForm.SERVICE_CHOICE_1
                                                                                                     if
                                                                                                     key == service_id][
                                                                                                        0], text)
    send_text = 'https://api.telegram.org/bot' + BOT_TOKEN + '/sendMessage?chat_id=' + BOT_CHAT_ID + '&parse_mode=Markdown&text=' + msg_to_telegramm
    requests.get(send_text)
    return redirect('main:thanks')


@minified_response
def shares(request):
    ctx = {}
    ctx['sales'] = get_sales()
    test = [('Головна', 'main:home'), ('Акции', '')]
    ctx['bredcrumbs'] = test
    return render(request, 'main/shares.html', ctx)


@minified_response
def detail(reques, pk):
    test = [('Головна', 'main:home'), ('Акции', 'main:shares')]
    sale = Sale.objects.get(pk=pk).title
    test.append((sale, ''))
    return render(reques, 'main/test.html', {
        'bredcrumbs': test
    })


@minified_response
def category(request, slug):
    ctx = {}
    test = [('Головна', 'main:home'), ('Цены', 'main:services')]
    category = Category.objects.get(slug=slug)
    test.append((category.name, ''))
    ctx['category'] = category
    ctx['bredcrumbs'] = test
    return render(request, 'main/category.html', ctx)


@minified_response
def services(request):
    ctx = {}
    test = [('Головна', 'main:home'), ('Цены', '')]
    ctx['bredcrumbs'] = test
    return render(request, 'main/services.html', ctx)
