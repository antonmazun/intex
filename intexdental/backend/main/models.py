from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext_noop as _noop
from django.utils import timezone
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from pytils.translit import slugify

from django.core.exceptions import ValidationError

from adminsortable.models import SortableMixin

# Create your models here.


class Sale(models.Model):
	photo = models.ImageField(upload_to='image_sales/', verbose_name='Фото акции', default='', blank=True, null=True)
	title = models.CharField(max_length=255, verbose_name='Заголовок акции')
	text = RichTextField()
	status = models.BooleanField(default=True, verbose_name='Акция еще действительна?')

	def __str__(self):
		return '{}'.format(self.title)

	class Meta:
		verbose_name = 'Акция'
		verbose_name_plural = 'Акции'


import xml.etree.cElementTree as et


def validate_svg(f):
	# Find "start" word in file and get "tag" from there
	f.seek(0)
	tag = None
	ext = f.name.split('.')[-1]
	if ext in ['png', 'jpg', 'jpeg', 'gif']:
		return f
	print(f, type(f), 'ffffff')
	try:
		for event, el in et.iterparse(f, ('start',)):
			print('event el', event, el, el.tag)
			tag = el.tag
			break
	except et.ParseError:
		pass

	# Check that this "tag" is correct
	if tag != '{http://www.w3.org/2000/svg}svg':
		raise ValidationError('Uploaded file is not an image or SVG file.')

	# Do not forget to "reset" file
	f.seek(0)

	return f


class Category(SortableMixin):
	name = models.CharField(max_length=254, verbose_name='Категории услуги')
	icon_first = models.FileField(upload_to='category_icons/', blank=True, verbose_name='Икона ДО наведения', null=True, default='', validators=[validate_svg])
	icon_hover = models.FileField(upload_to='category_icons/', blank=True, verbose_name='Икона ПОСЛЕ наведения', null=True, default='', validators=[validate_svg])
	status = models.BooleanField(default=True, verbose_name='Показывать на сайте')
	slug = models.SlugField(max_length=254, unique=True, blank=True, null=True)
	the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.name) + str(self.id) if self.id else ''
			super(Category, self).save(*args, **kwargs)
		super(Category, self).save(*args, **kwargs)

	class Meta:
		verbose_name = 'категория услуги'
		verbose_name_plural = 'категории услуг'
		ordering = ['the_order']

	def get_sub_category(self):
		return SubCategory.objects.filter(category=self, status=True)

	def get_subcategory_all(self):
		return SubCategory.objects.filter(category=self)


class SubCategory(SortableMixin):
	category = models.ForeignKey(Category, on_delete=models.CASCADE)
	name = models.CharField(max_length=254, verbose_name='Название услуги')
	price = models.CharField(default=0, verbose_name='Цена услуги', max_length=200)
	status = models.BooleanField(default=True, verbose_name='Показывать на главной странице')
	desc = RichTextUploadingField(default='')
	slug = models.SlugField(max_length=254, unique=True, blank=True, null=True)
	the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
	# def save(self, *args, **kwargs):
	# 	if not self.slug:
	# 		print('not slug!')
	# 	else:
	# 		self.slug = slugify(self.name) + str(self.id) if self.id else ''
	# 	super(SubCategory, self).save(*args, **kwargs)
	# 	#super(SubCategory, self).save(*args, **kwargs)

	def photo(self):
		return PhotoAfterBefore.objects.filter(service=self)

	def __str__(self):
		return '{category} ---> {name}'.format(category=self.category.name, name=self.name)

	def get_subcategory(self):
		return SubSubCategory.objects.filter(subcategory=self)

	class Meta:
		verbose_name = 'услуга'
		verbose_name_plural = 'услуги'
		ordering = ['the_order']

class SubSubCategory(models.Model):
	subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
	title = models.CharField(max_length=255, verbose_name='Вид услуги')
	price = models.CharField(max_length=255, verbose_name='Цена услуги')


class StaticInfo(SortableMixin):
	name = models.CharField(max_length=254, verbose_name='Название услуги')
	price = models.CharField(default=0, verbose_name='Цена услуги', max_length=100, blank=True, null=True)
	icon_first = models.FileField(upload_to='category_icons/', blank=True, verbose_name='Икона ДО наведения', null=True, default='', validators=[validate_svg])
	icon_hover = models.FileField(upload_to='category_icons/', blank=True, verbose_name='Икона ПОСЛЕ наведения', null=True, default='', validators=[validate_svg])
	status = models.BooleanField(default=True, verbose_name='Показывать на главной странице')
	desc = RichTextUploadingField(default='')
	slug = models.SlugField(max_length=254, unique=True, blank=True, null=True)
	the_order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
	image  = models.ImageField(upload_to='static_info_image/' , blank=True , null=True , default='')

	def __str__(self):
		return 'Static text ' + self.name

	def save(self, *args, **kwargs):
		try:
			if not self.slug:
				self.slug = slugify(self.name)
		except Exception:
			if not self.slug:
				self.slug = slugify(self.name) + str(self.id) if self.id else ''
		super(StaticInfo, self).save(*args, **kwargs)

	class Meta:
		verbose_name = 'Квадратик на главной странице'
		verbose_name_plural = 'Квадратики на главной странице'
		ordering = ['the_order']


class Doctor(models.Model):
	fio = models.CharField(max_length=254, verbose_name='ФИО доктора')
	occupation = models.CharField(max_length=300, verbose_name='Должность', blank=True, null=True)
	desc = RichTextUploadingField(verbose_name='О враче', blank=True, null=True)
	services = models.ManyToManyField(SubCategory, verbose_name='Какие услуги предоставляет врач?', blank=True, null=True)
	photo = models.ImageField(upload_to='doctors/', blank=True, null=True)
	status = models.BooleanField(default=True, verbose_name='Показывать на сайте?')
	slug = models.SlugField(max_length=254, unique=True, blank=True, null=True)
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='meta')
	meta_keywords = models.TextField(verbose_name='мета keywords', default='meta')

	def save(self, *args, **kwargs):
		try:
			if not self.slug:
				self.slug = slugify(self.fio)
		except Exception as e:
			if not self.slug:
				self.slug = slugify(self.fio) + str(self.id) if self.id else ''
		super(Doctor, self).save(*args, **kwargs)

	def get_articles(self):
		return Article.objects.filter(author=self, status=True).order_by("-id")

	def get_certificates(self):
		return Certificate.objects.filter(doctor=self)

	def __str__(self):
		return self.fio

	class Meta:
		verbose_name = 'Доктор'
		verbose_name_plural = 'Докторa'


class Certificate(models.Model):
	doctor = models.ForeignKey(Doctor, on_delete=models.SET_NULL, blank=True, null=True)
	img = models.ImageField(upload_to='certificate_image/')


class CallBackForm(models.Model):
	SERVICE_CHOICE_1 = (
		('priem' , 'Записаться на прием'),
		('consult_terapevt' , 'Консультация терапевта'),
		('consult_ortoped' , 'Консультация ортопеда'),
		('consult_implant' , 'Консультация по вопросам имплантации'),
		('gigiena' , 'Профессиональная гигиена'),
	)

	name_user = models.CharField(max_length=255, verbose_name=_('Имя пациента'))
	phone_number = models.CharField(max_length=255, verbose_name=_('Номер телефона'))
	msg = models.TextField(max_length=5000, verbose_name=_('Текст сообщения'), blank=True, null=True)
	date = models.DateTimeField(default=timezone.now)
	status = models.BooleanField(default=False, verbose_name='Обработана заявка')
	service = models.ForeignKey(SubCategory, on_delete=models.SET_NULL, blank=True, null=True, default=None)
	doctor = models.ForeignKey(Doctor, on_delete=models.SET_NULL, blank=True, null=True, default=None)
	service_1  = models.CharField(max_length=255 , choices=SERVICE_CHOICE_1 , default='')
	def __str__(self):
		return '{} {}'.format(self.name_user, self.phone_number)

	class Meta:
		verbose_name = 'Заявка'
		verbose_name_plural = 'Заявки'


class PhotoAfterBefore(models.Model):
	service = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
	before_photo = models.ImageField(upload_to='after_before/', blank=True, null=True, verbose_name='Фото ')
	after_photo = models.ImageField(upload_to='after_before/', blank=True, null=True, verbose_name='Фото')

	def __str__(self):
		return '{}'.format(self.service)


class RecomendBlock(models.Model):
	title = models.CharField(max_length=200, verbose_name='Заголовок рекомендации')
	text = models.TextField(max_length=1000, verbose_name='Текст рекомендации')
	status = models.BooleanField(default=True, verbose_name='Показывать эту рекомендацию')

	def __str__(self):
		return 'Заголовок - {} Текст - {}'.format(self.title, self.text)

	class Meta:
		verbose_name = 'Рекомендация'
		verbose_name_plural = 'Рекомендации (возле зубика на главной странице)'


class Article(models.Model):
	author = models.ForeignKey(Doctor, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Автор статьи')
	title = models.CharField(max_length=254, verbose_name='Заголовок статьи', blank=True, null=True)
	photo = models.ImageField(upload_to='articles/', blank=True, null=True, default='')
	lead = models.CharField(max_length=300, verbose_name='Аннотация (краткое описание)', blank=True, null=True)
	content = RichTextUploadingField()
	status = models.BooleanField(default=True, verbose_name='Показывать статью?')
	date = models.DateField(auto_now_add=True, null=True, blank=True)
	articles = models.ManyToManyField('self', blank=True, null=True)
	slug = models.SlugField(max_length=254, unique=True, blank=True, null=True)
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='', blank=True)
	meta_keywords = models.TextField(verbose_name='мета keywords', default='', null=True)

	def save(self, *args, **kwargs):
		try:
			if not self.slug:
				self.slug = slugify(self.title)
				super(Article, self).save(*args, **kwargs)
		except Exception as e:
			if not self.slug:
				self.slug = slugify(self.title) + str(self.id) if self.id else ''
				super(Article, self).save(*args, **kwargs)
		super(Article, self).save(*args, **kwargs)

	def __str__(self):
		return '{} {}'.format(self.author, self.title)


class Review(models.Model):
	name = models.CharField(max_length=255, verbose_name='Имя человека')
	text = models.TextField(max_length=1000, verbose_name='Текст')
	status = models.BooleanField(default=False, verbose_name='Показать отзыв на сайте?')
	date = models.DateTimeField(auto_now_add=True, null=True, blank=True)


class Question(models.Model):
	question = models.CharField(max_length=500, verbose_name='Вопрос')
	answer = models.TextField(max_length=1500, verbose_name='Ответ')
	content_answer  = RichTextUploadingField(default='' , blank=True, null=True)
	status  = models.BooleanField(default=True)


class Gallery(models.Model):
	image = models.ImageField(upload_to='gallery_image/')
