from django.urls import path, include
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.homepage, name='home'),
    path('review/', views.reviewpage, name='review'),
    path('gallery/', views.gallerypage, name='gallery'),
    path('questions/', views.questionspage, name='questions'),
    path('contacts/', views.contactspage, name='contacts'),
    path('price/', views.pricepage, name='price'),
    path('service/<slug:slug>/', views.servicepage, name='service'),
    path('doctors/', views.doctorspage, name='doctors'),
    path('doctor/<slug:slug>/', views.doctorpage, name='doctor'),
    path('stocks/', views.stockspage, name='stocks'),
    path('articles/', views.articlespage, name='articles'),
    path('article/<slug:slug>', views.articlepage, name='article'),
    path('shares/', views.shares, name='shares'),
    path('services/', views.services, name='services'),
    path('detail/<int:pk>/', views.detail, name='detail'),
    path('category/<slug:slug>/', views.category, name='category'),
]

form_urls = [
    path('call-back-form/', views.form, name='form'),
    path('callback_full/', views.callback_full, name='callback_full'),
    path('thanks/', views.thanks, name='thanks'),
]

urlpatterns += form_urls
