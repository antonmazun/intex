from django import template
from django.conf import settings
from ..forms import CallBackFormFront
from ..models import Category, Sale, RecomendBlock,\
    SubCategory, Doctor, Review , Question , StaticInfo , Gallery , CallBackForm

register = template.Library()


@register.simple_tag
def get_language(request):
    # print("get_language", request.get_full_path())
    print('request.LANGUAGE_CODE', request.LANGUAGE_CODE)
    return {
        'LANGUAGES': settings.LANGUAGES,
        'SELECTEDLANG': request.LANGUAGE_CODE,
        'redirect_to': request.get_full_path()[3:]
    }


@register.simple_tag
def get_callback_form():
    return {
        'form': CallBackFormFront()
    }


@register.simple_tag
def get_category():
    return SubCategory.objects.filter(status=True)

@register.simple_tag
def get_services():
    return CallBackForm.SERVICE_CHOICE_1

@register.simple_tag
def get_sales():
    return Sale.objects.all()


@register.simple_tag
def get_doctors():
    return Doctor.objects.filter(status=True)


@register.simple_tag
def get_recomend():
    return RecomendBlock.objects.order_by('-id').filter(status=True)[:4]


@register.simple_tag
def get_reviews():
    return Review.objects.filter(status=True).order_by('-id')

@register.simple_tag
def get_questions():
    return Question.objects.all()

@register.simple_tag
def get_questions_home_page():
    return Question.objects.filter(status=True)

@register.simple_tag
def get_static_info_main_page():
    return StaticInfo.objects.filter(status=True)


@register.simple_tag
def get_gallery():
    return Gallery.objects.all()