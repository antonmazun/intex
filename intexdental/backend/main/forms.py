from django import forms
# from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy as _, ugettext_noop as _noop
from .models import CallBackForm


class CallBackFormFront(forms.ModelForm):
    class Meta:
        model = CallBackForm
        # fields = '__all__'
        exclude = ['date', 'status', 'doctor', 'service' , 'service_1']

    def __init__(self, *args, **kwargs):
        super(CallBackFormFront, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if isinstance(field.widget, forms.TextInput) or \
                    isinstance(field.widget, forms.Textarea) or \
                    isinstance(field.widget, forms.DateInput) or \
                    isinstance(field.widget, forms.DateTimeInput) or \
                    isinstance(field.widget, forms.TimeInput):
                field.widget.attrs.update({'placeholder': field.label})
