from modeltranslation.translator import translator, TranslationOptions, register
from .models import Sale, Category, SubCategory,\
    RecomendBlock, Article, Doctor,\
    Question , SubSubCategory , StaticInfo


@register(Sale)
class SaleTranslation(TranslationOptions):
    fields = ('title', 'text')
    required_languages = ('ru', 'ua')


@register(Article)
class ArticleTranslation(TranslationOptions):
    fields = ('title', 'lead', 'content' , 'meta_desc' , 'meta_keywords')
    required_languages = ('ru', 'ua')


@register(Category)
class CategoryTranslation(TranslationOptions):
    fields = ('name',)
    required_languages = ('ru', 'ua')


@register(SubCategory)
class SubCategoryTranslation(TranslationOptions):
    fields = ('name',)
    required_languages = ('ru', 'ua')


@register(RecomendBlock)
class RecomendBlockTranslation(TranslationOptions):
    fields = ('title', 'text')
    required_languages = ('ru', 'ua')


@register(Doctor)
class DoctorTransalte(TranslationOptions):
    fields = ('fio', 'occupation', 'desc' , 'meta_desc' , 'meta_keywords')
    required_languages = ('ru', 'ua')


@register(Question)
class QuestionTranslate(TranslationOptions):
    fields  = ('question' , 'answer')
    required_languages = ('ru', 'ua')


@register(SubSubCategory)
class SubSubCategoryTranslate(TranslationOptions):
    fields  = ('title' , )
    required_languages = ('ru', 'ua')


@register(StaticInfo)
class StaticInfoAdmin(TranslationOptions):
    fields  = ('name' , 'desc' , 'price')
    required_languages = ('ru', 'ua')