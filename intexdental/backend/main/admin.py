from django.contrib import admin
from .models import CallBackForm, Sale, Category, SubCategory, \
    RecomendBlock, PhotoAfterBefore, Doctor, Article, \
    Review, Question, SubSubCategory, StaticInfo, Certificate, Gallery
from .svg_field import SVGAndImageFormField
from adminsortable.admin import SortableAdmin
from django import forms
from adminsortable.admin import NonSortableParentAdmin, SortableStackedInline


class SubCategoryInline(admin.StackedInline):
    model = SubCategory
    extra =0
    # max_num = 1

class MyModelForm(forms.ModelForm):
    class Meta:
        model = Category
        exclude = []
        field_classes = {'icon': SVGAndImageFormField, }


@admin.register(Category)
class CategoryAdmin(SortableAdmin):
    # inlines = [SubCategoryInline]
    list_display = ['name', 'status']
    list_editable = ['status']
    prepopulated_fields = {"slug": ("name",)}


class SubSubCategoryInline(admin.StackedInline):
    model = SubSubCategory


@admin.register(SubCategory)
class SubCategoryAdmin(SortableAdmin):
    list_display = ['id', 'name', 'price', 'status']
    list_editable = ['name', 'price', 'status']
    list_filter = ['category']
    prepopulated_fields = {"slug": ("name",)}
    inlines = [SubSubCategoryInline]


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'text', 'status']
    list_editable = ['name', 'text', 'status']
    list_display_links = ['id']
    list_filter = ['date', 'status']


class CertificateInline(admin.StackedInline):
    model = Certificate


@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("fio",)}
    list_display = ['id', 'fio', 'occupation', 'status']
    list_display_links = ['id']
    list_editable = ['fio', 'occupation', 'status']
    inlines = [CertificateInline, ]


# Register your models here.

@admin.register(CallBackForm)
class CallBackFormAdmin(admin.ModelAdmin):
    list_display = ['name_user', 'phone_number', 'msg', 'date', 'status']
    list_editable = ('status',)
    list_filter = ['date', 'status']


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = ['title', 'status']
    list_editable = ('status',)
    list_filter = ['title', 'status']


@admin.register(RecomendBlock)
class RecomendBlockAdmin(admin.ModelAdmin):
    list_display = ['title', 'status']
    list_editable = ('status',)
    list_filter = ['status']


@admin.register(PhotoAfterBefore)
class PhotoAfterBeforeAdmin(admin.ModelAdmin):
    pass


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'author', 'date', 'status']
    list_filter = ['author', 'status']
    list_editable = ('status',)
    pass


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    pass


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ['id', 'question', 'answer']
    list_editable = ['question', 'answer']


@admin.register(SubSubCategory)
class SubSubCategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'price', 'subcategory']
    list_filter = ['subcategory']


class StaticInfoAdmin(SortableAdmin):
    list_display = ['id', 'name', 'status', 'price']
    list_editable = ['status']
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(StaticInfo, StaticInfoAdmin)  # @admin.register(StaticInfo)
# class StaticInfoAdmin(admin.ModelAdmin):
# 	pass
# list_display = ['id', 'name', 'status', 'price']
# list_editable = ['status']
# prepopulated_fields = {"slug": ("name",)}
