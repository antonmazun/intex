# # myapp/telegrambot.py
# # Example code for telegrambot.py module
# from telegram.ext import CommandHandler, MessageHandler, Filters
# from django_telegrambot.apps import DjangoTelegramBot
#
# import logging
#
# logger = logging.getLogger(__name__)
#
#
# # Define a few command handlers. These usually take the two arguments bot and
# # update. Error handlers also receive the raised TelegramError object in error.
# def start(bot, update):
#     print('start!!!!')
#     bot.sendMessage(update.message.chat_id, text='Hi!')
#
#
# def help(bot, update):
#     bot.sendMessage(update.message.chat_id, text='Help!')
#
#
# def echo(bot, update):
#     bot.sendMessage(update.message.chat_id, text=update.message.text)
#
#
# def error(bot, update, error):
#     logger.warning('Update "%s" caused error "%s"' % (update, error))
#
#
# def main():
#     logger.info("Loading handlers for telegram bot")
#
#     # Default dispatcher (this is related to the first bot in settings.DJANGO_TELEGRAMBOT['BOTS'])
#     dp = DjangoTelegramBot.dispatcher
#     # To get Dispatcher related to a specific bot
#     dp = DjangoTelegramBot.getDispatcher('1005872507:AAHwfrZzRDPMGTo5ocQWh9U8OY70dI6e-bI')  # get by bot token
#     dp = DjangoTelegramBot.getDispatcher('run_test_antonmazun_bot')  # get by bot username
#
#     # on different commands - answer in Telegram
#     dp.add_handler(CommandHandler("start", start))
#     dp.add_handler(CommandHandler("help", help))
#
#     # on noncommand i.e message - echo the message on Telegram
#     try:
#
#         dp.add_handler(MessageHandler([Filters.text], echo))
#     except Exception as e:
#         print(e, type(e))
#
#     # log all errors
#     # dp.add_error_handler(error)
