from modeltranslation.translator import translator, TranslationOptions, register
from . import models


@register(models.MetaHome)
class MetaHomeTranslationsOption(TranslationOptions):
	fields = ('meta_desc', 'meta_keywords')
	required_languages = ('ru', 'ua')


@register(models.MetaShare)
class MetaShareTranslationsOption(TranslationOptions):
	fields = ('meta_desc', 'meta_keywords')
	required_languages = ('ru', 'ua')


@register(models.MetaPrice)
class MetaPriceTranslationsOption(TranslationOptions):
	fields = ('meta_desc', 'meta_keywords')
	required_languages = ('ru', 'ua')


@register(models.MetaCommand)
class MetaCommandTranslationsOption(TranslationOptions):
	fields = ('meta_desc', 'meta_keywords')
	required_languages = ('ru', 'ua')


@register(models.MetaReview)
class MetaReviewTranslationsOption(TranslationOptions):
	fields = ('meta_desc', 'meta_keywords')
	required_languages = ('ru', 'ua')



@register(models.MetaBlog)
class MetaBlogTranslationsOption(TranslationOptions):
	fields = ('meta_desc', 'meta_keywords')
	required_languages = ('ru', 'ua')


@register(models.MetaContact)
class MetaContactTranslationsOption(TranslationOptions):
	fields = ('meta_desc', 'meta_keywords')
	required_languages = ('ru', 'ua')


@register(models.MetaGallery)
class MetaGalleryTranslationsOption(TranslationOptions):
	fields = ('meta_desc', 'meta_keywords')
	required_languages = ('ru', 'ua')
