from django.db import models
from solo.models import SingletonModel


# Create your models here.

class MetaSolo(models.Model):
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='meta')
	meta_keywords = models.TextField(verbose_name='мета keywords', default='meta')
	t  = models.CharField(blank=True, null=True , max_length=12)
	def __str__(self):
		return self.meta_keywords


class MetaHome(SingletonModel):
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='meta')
	meta_keywords = models.TextField(verbose_name='мета keywords', default='meta')

	def __str__(self):
		return 'Главная страница'

	class Meta:
		verbose_name = 'Мета теги для главной страницы'


class MetaShare(SingletonModel):
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='meta')
	meta_keywords = models.TextField(verbose_name='мета keywords', default='meta')

	def __str__(self):
		return 'Страница Акции'

	class Meta:
		verbose_name = 'Мета теги для страницы Акции'


class MetaPrice(SingletonModel):
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='meta')
	meta_keywords = models.TextField(verbose_name='мета keywords', default='meta')

	def __str__(self):
		return 'Страница услуги'

	class Meta:
		verbose_name = 'Мета теги для страницы Услуги'


class MetaCommand(SingletonModel):
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='meta')
	meta_keywords = models.TextField(verbose_name='мета keywords', default='meta')

	def __str__(self):
		return 'Страница "наша команада"'

	class Meta:
		verbose_name = 'Мета теги для страницы "наша команада"'



class MetaReview(SingletonModel):
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='meta')
	meta_keywords = models.TextField(verbose_name='мета keywords', default='meta')

	def __str__(self):
		return 'Страница "Отзывы"'

	class Meta:
		verbose_name = 'Мета теги для страницы "Отзывы"'

class MetaGallery(SingletonModel):
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='meta')
	meta_keywords = models.TextField(verbose_name='мета keywords', default='meta')

	def __str__(self):
		return 'Страница "Галерея"'

	class Meta:
		verbose_name = 'Мета теги для страницы "Галерея"'

class MetaBlog(SingletonModel):
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='meta')
	meta_keywords = models.TextField(verbose_name='мета keywords', default='meta')

	def __str__(self):
		return 'Страница "Все статьи"'

	class Meta:
		verbose_name = 'Мета теги для страницы "Все статьи"'

class MetaContact(SingletonModel):
	meta_desc = models.TextField(verbose_name='мета дескрипшни', default='meta')
	meta_keywords = models.TextField(verbose_name='мета keywords', default='meta')

	def __str__(self):
		return 'Страница "Контакты"'

	class Meta:
		verbose_name = 'Мета теги для страницы "Контакты"'

