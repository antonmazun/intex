from django.contrib import admin

# Register your models here.
from .models import MetaHome, MetaSolo, MetaShare, MetaPrice, MetaCommand, MetaReview, MetaGallery, MetaBlog, MetaContact
from solo.admin import SingletonModelAdmin

# admin.site.register(MetaHome, SingletonModelAdmin)
admin.site.register(MetaShare, SingletonModelAdmin)
admin.site.register(MetaPrice, SingletonModelAdmin)
admin.site.register(MetaHome, SingletonModelAdmin)
admin.site.register(MetaCommand, SingletonModelAdmin)
admin.site.register(MetaReview, SingletonModelAdmin)
admin.site.register(MetaGallery, SingletonModelAdmin)
admin.site.register(MetaBlog, SingletonModelAdmin)
admin.site.register(MetaContact, SingletonModelAdmin)

#
# @admin.register(MetaHome)
# class MetaHomeAdmin(admin.ModelAdmin):
#     def get_queryset(self, request):
#         qs = super(MetaHomeAdmin, self).get_queryset(request)
#         return qs
#
#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         print(' formfield_for_choice_field formfield_for_choice_field formfield_for_choice_fieldformfield_for_choice_field')
#         if db_field.name == "meta":
#             kwargs["queryset"] = MetaSolo.objects.all().exclude(id__in=[MetaSolo.objects.all()])
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)
#
#
# admin.site.register(MetaSolo)
