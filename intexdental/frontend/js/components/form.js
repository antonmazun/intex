import $ from 'jquery';

export default class CallBackForm {
    constructor() {
        this.forms = $('.js-form-submit');
        this.form_class = {
            'hidden': 'hidden',
            'visible': 'visible'
        };
        this.thx_text = $('.js-thx');
        this.validate = {
            'name': function (value) {
                return value ? value.length < 255 : false; // 256
            }
        }
    }

    submit_form(forms) {
        let self = this;
        $(forms).on('submit', function (e) {
            let form = this;
            e.preventDefault();
            let form_data = new FormData(this);
            let csrf_token = $(this).find('input[name="csrfmiddlewaretoken"]').val();
            let name_input = $(this).find('input[name="name_user"]');
            if (!self.validate['name'](name_input.val())) {
                let data_to_input = $(name_input).attr('name');
                $(`input[name="${data_to_input}"]`).addClass('error');
                $(`.js-error-field[data-name="${data_to_input}"]`).removeClass('hide');
                return;
            }
            $.ajax({
                method: this.method,
                url: this.action,
                headers: {"X-CSRFToken": csrf_token},
                processData: false,
                contentType: false,
                dataType: "json",
                data: form_data
            }).done(function (response) {
                if (response.status) {
                    $(form).addClass(self.form_class['hidden']);
                    self.thx_text.removeClass('hide');
                }
            })
        })
    }

    init() {
        this.submit_form(this.forms);
    }

    review_form_ajax() {
        $('.js-review-form').on('submit', function (e) {
            e.preventDefault();
            let form = $(this);
            console.log(form.attr('action'));
            console.log(form.attr('method'));
            let csrf_token = form.find('input[name="csrfmiddlewaretoken"]').val();
            let name = form.find('input[name="name"]').val();
            let text = form.find('textarea[name="text"]').val();
            $.ajax({
                method: this.method,
                url: this.action,
                headers: {"X-CSRFToken": csrf_token},
                data: {
                    'name': name,
                    'text': text,
                }
            }).done(function (response) {
                if (response.status) {
                    form.addClass('hidden');
                    $('.js-form-help').removeClass('hide');
                }
                console.log(response);
            })
        })
    }

    callbackFormFull() {
        // alert('asdasdasd');
        $('.js-form-submit').on('submit', function (e) {
            e.preventDefault();
            let form = $(this);
            let name = form.find('input[name="name"]').val();
            let phone = form.find('input[name="phone"]').val();
            let service_id = form.find('select[name="service_id"]').val();
            let doctor_id = form.find('select[name="doctor_id"]').val();
            let text = form.find('textarea[name="text"]').val();
            let csrf_token = $(this).find('input[name="csrfmiddlewaretoken"]').val();
            $.ajax({
                method: this.method,
                url: this.action,
                headers: {"X-CSRFToken": csrf_token},
                data: {
                    'name': name,
                    'phone': phone,
                    'service_id': service_id,
                    'doctor_id': doctor_id,
                    'text': text,
                }
            }).done(function (response) {
                if (response.status) {
                    console.log($(form)[0]);
                    $(form).addClass('hidden');
                    $('.js-form-help').removeClass('hide');
                }
            });
        })
    }

}
