import $ from 'jquery';

export default class Question {
    constructor() {

    }

    init() {
        this.activeQuestion();
    }


    activeQuestion() {
        let heightActive = document.querySelector('.question.active .question-answer').scrollHeight + 'px';
        document.querySelector('.question.active .question-answer').style.height = heightActive;
        let questions = document.querySelectorAll('.question');
        questions.forEach(element => {
            element.addEventListener("click", function () {
                if (this.classList.contains("active")) {
                    this.classList.remove('active');
                    this.querySelector('.question-answer').style.height = 0;
                } else {
                    let height = this.querySelector('.question-answer').scrollHeight + 'px';
                    this.querySelector('.question-answer').style.height = height;
                    this.classList.add('active');
                }
            });
        });
    }

}
