import $ from "jquery";
import jQuery from 'jquery';
import mCustomScrollbar from 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';

export default class Home {
    constructor() {
        this.services = $('.services-doctor');
    }

    init() {
        this.initScrollbar_vertical(this.services);
        this.initRecommend();

        $('[data-page="home"] header').removeClass('header-white');
        $('[data-page="home"] .img-logoG').addClass('display-none');
        $('[data-page="home"] .img-logoW').removeClass('display-none');
    }

    initScrollbar_vertical(selectorName) {
        (function ($) {
            $(window).on("load", function () {
                $(selectorName).mCustomScrollbar({
                    theme: 'dark',
                    scrollInertia: 300,
                    scrollEasing: 'linear',
                    axis: "y",
                    mouseWheel: {
                        enable: true
                    },
                    documentTouchScroll: true,
                });
            });
        })(jQuery);
    }

    initRecommend() {
        let recom = $('.point');
        $('.recom').each(function (index, element) {
            let curent_height_block = $(element).innerHeight();
            $(element).css({
                'top': -(curent_height_block + 33) + 'px',
            });
            console.log(index, curent_height_block);
        });
        $(recom).on('click', function () {
            $(this).find('.recom').toggleClass('display_none');
            $(this).toggleClass('active_recom');
        })
    }

}
