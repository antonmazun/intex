import $ from "jquery";
import jQuery from 'jquery';
import 'slick-carousel';

export default class Slider {
    constructor() {

    }

    init() {

    }

    initDoctorPage() {
        let certificate = $('.img-slider-certificate');
        let doctors = $('.slider-doctor-articles');
        let main_selector_cert = '.certificate-doctor';
        let main_selector_doc = '.articles-doctor';

        this.initCertificateSlider(certificate, 5, 4, 3, 2, main_selector_cert);
        this.initCertificateSlider(doctors, 3, 3, 2, 1, main_selector_doc);
    }

    initHomePage() {
        let sale = $('.sale-slider-wrap');
        let main_selector_sale = '.sale-slider';
        let review = $('.review-slider');
        let main_selector_review = '.review-slider-wrap';
        this.initCertificateSlider(sale, 1, 1, 1, 1, main_selector_sale);
        this.initReviewsSlider(review, 1, 1, 1, 1, main_selector_review);

    }


    initCertificateSlider(certificate, count1, count2, count3, count4, main_selector) {
        $(certificate).slick({
            dots: true,
            infinite: false,
            speed: 600,
            slidesToShow: count1,
            slidesToScroll: count1,
            adaptiveHeight: true,
            arrows: true,
            touchMove: true,
            responsive: [
                {
                    breakpoint: 1100,
                    settings: {
                        slidesToShow: count2,
                        slidesToScroll: count2,
                        infinite: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: count3,
                        slidesToScroll: count3,
                        infinite: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: count4,
                        slidesToScroll: count4,
                        infinite: false,
                        dots: true
                    }
                }
            ]
        });
        $(`${main_selector} .slick-dots li`).last().clone().appendTo(`${main_selector} .slick-dots`);

    }

    initReviewsSlider(certificate, count1, count2, count3, count4, main_selector) {
        $(certificate).slick({
            dots: true,
            infinite: true,
            speed: 600,
            centerMode: true,
            centerPadding: '450px',
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            touchMove: true,
            adaptiveHeight: false,
            responsive: [
                {
                    breakpoint: 1850,
                    settings: {
                        slidesToShow: count1,
                        centerPadding: '450px',
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 1250,
                    settings: {
                        slidesToShow: count2,
                        centerPadding: '250px',
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 1050,
                    settings: {
                        slidesToShow: count2,
                        centerPadding: '150px',
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: count3,
                        centerMode: false,
                        centerPadding: '0px',
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: count3,
                        centerMode: false,
                        centerPadding: '15px',
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                }
            ]
        });
        $(`${main_selector} .slick-dots li`).last().clone().appendTo(`${main_selector} .slick-dots`);

    }

}
