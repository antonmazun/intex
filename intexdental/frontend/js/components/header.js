import $ from 'jquery';

export default class Header {
    constructor() {
    }

    init() {
        $(window).scroll(function () {

            if ($(this).scrollTop() > 10) {
                $('header').addClass("header-white");
                $('.img-logoW').addClass("display-none");
                $('.img-logoG').removeClass("display-none");

            } else {
                $('header').removeClass("header-white");
                $('.img-logoW').removeClass("display-none");
                $('.img-logoG').addClass("display-none");
            }

        });

    }

    mob_menu(){
        $('.icon-menu').click((e)=>{
            if($('.mob-menu').hasClass('open')){
                $('.mob-menu.open').removeClass('open');
            }else {
                $('.mob-menu').addClass('open');
            }
        });
    }

}
