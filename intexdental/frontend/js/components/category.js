import $ from 'jquery';

export default class Category {
    constructor() {
        this.category = $('.price-category');
    }

    init() {
        this.activeCategory(this.category);
        this.select();

    }

    activeCategory(category) {
        // let height_origin = 0;
        // $('.price-category.active .category-sub').map(function (index, elem) {
        //     height_origin += $(elem).outerHeight();
        // });
        // let height = $('.price-category.active .category-sub').length * $('.category-sub').outerHeight() + 'px';
        // let height = `${height_origin} px`;
        // $('.price-category.active .category-subs').height(height);
        $(category).on("click", function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).find('.category-subs').height(0);
            } else {
                let height_origin = 0;
                $(this).find('.category-sub').map(function (index, elem) {
                    console.log(elem);
                    height_origin += $(elem).outerHeight();
                })
                let height = height_origin;
                // let height = $(this).find('.category-sub').length * $('.category-sub').outerHeight() + 'px';
                // let height = $(this).find('.category-sub').offsetHeight()+ 'px';
                console.log($(this).find('.category-subs'));
                $(this).find('.category-subs').height(height);
                $(this).addClass('active');
            }
        });
    }

    select() {
        $('select').each(function () {
            var $this = $(this), numberOfOptions = $(this).children('option').length;

            $this.addClass('select-hidden');
            $this.wrap('<div class="select"></div>');
            $this.after('<div class="select-styled"></div>');

            var $styledSelect = $this.next('div.select-styled');
            $styledSelect.text($this.children('option').eq(0).text());

            var $list = $('<ul />', {
                'class': 'select-options'
            }).insertAfter($styledSelect);

            for (var i = 0; i < numberOfOptions; i++) {
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val()
                }).appendTo($list);
            }

            var $listItems = $list.children('li');

            $styledSelect.click(function (e) {
                e.stopPropagation();
                $('div.select-styled.active').not(this).each(function () {
                    $(this).removeClass('active').next('ul.select-options').hide();
                });
                $(this).toggleClass('active').next('ul.select-options').toggle();
            });

            $listItems.click(function (e) {
                e.stopPropagation();
                $styledSelect.text($(this).text()).removeClass('active');
                $this.val($(this).attr('rel'));
                $list.hide();
                //console.log($this.val());
            });

            $(document).click(function () {
                $styledSelect.removeClass('active');
                $list.hide();
            });

        });
    }


}
