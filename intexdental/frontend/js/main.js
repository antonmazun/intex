import Home from './components/home';
import Slider from './components/slider';
import Jquery from 'jquery'
import CallBackForm from './components/form';
import Header from './components/header';
import Map from './components/map';
import Category from './components/category';
import Question from './components/question';
import InputMask from 'inputmask';
import $ from 'jquery';

class Root {
    constructor() {
        this.home = new Home();
        this.slider = new Slider();
        this.form = new CallBackForm();
        this.header = new Header();
        this.map = new Map();
        this.category = new Category();
        this.question = new Question();
    }

    phone_mask(id_input) {
        var selector = document.getElementById(id_input);
        var im = new InputMask("+38 (099) 999-99-99");
        im.mask(selector);
    }

    scrool_to_block() {
        $('.js-anchor').click(function (e) {
            e.preventDefault();
            let to = $(this).data('to');
            console.log(to);
            document.getElementById(to).scrollIntoView({behavior: 'smooth'})
        })
    }

// function scrollToAnchor(id) {
//     document.getElementById(id).scrollIntoView({ behavior: 'smooth' });
//     mobileMenu.style.display = 'none';
//     removeDropdown();
// }
}


var App = new Root();
window.App = App;

window.$ = $;
